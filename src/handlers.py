import kopf
import time


# OpenWeatherMap API
from pyowm import OWM
from pyowm.utils import config
from pyowm.utils import timestamps


owm = OWM('565185408701b24d61631abb5280daf5')
mgr = owm.weather_manager()


def get_weather(location):
    observation = mgr.weather_at_place(location)
    w = observation.weather

    t = w.temperature('celsius')
    return f"{location} Status: {w.detailed_status} current: {t['temp']} max: {t['temp_max']} min: {t['temp_min']}"


@kopf.on.create('yalazi.org', 'v1', 'weathers')
def create_fn(spec, **kwargs):
    w = get_weather(spec['location'])
    print(f"New location {w}")
    return {'last_temp': w}

@kopf.timer('yalazi.org', 'v1', 'weathers', interval=15.0, idle=30)
def reconcile_weather(spec, **kwargs):

    w = get_weather(spec['location'])
    print(f"Reconciling: {w}")

    return {'last_temp': w}

@kopf.on.delete('yalazi.org', 'v1', 'weathers')
def create_fn(spec, **kwargs):
    print(f"Bye Bye {spec['location']}. ")
