FROM python:3.7
ADD src/. /src
RUN pip install -r src/requirements.txt
CMD kopf run /src/handlers.py --verbose
